package com.example.meteo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.meteo.models.current.OpenWeatherMap;
import com.example.meteo.models.forcast5.OpenwMap5Days;
import com.example.meteo.utils.Constant;
import com.example.meteo.utils.FastDialog;
import com.example.meteo.utils.Network;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;


public class MainActivity extends AppCompatActivity {

    private EditText editTextCity;
    private TextView textViewCity;
    private TextView textViewTemperature;
    private ImageView imageViewIcone;
    private ListView listViewForecast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextCity = (EditText) findViewById(R.id.editTextCity);
        textViewCity = (TextView) findViewById(R.id.textViewCity);
        textViewTemperature = (TextView) findViewById(R.id.textViewTemperature);
        imageViewIcone = (ImageView) findViewById(R.id.imageViewIcone);
        listViewForecast = (ListView) findViewById(R.id.listViewForecast);

    }

    public void check(View view) {

        if (!editTextCity.getText().toString().isEmpty()) { // n est pas vide grace au ! au début

            if (Network.isNetworkAvailable(MainActivity.this)) {
                // TODO : requete HTTP avec volley
                getVolley(Constant.PARAM_CURRENT);  // on modifie le parametre %s de la constqnte URL_METEO pour soit weather dans current ou forcast dans l autre
            }
        } else {
            FastDialog.showDialog(MainActivity.this,
                    FastDialog.SIMPLE_DIALOG,
                    "Vous devez être connecté"
            );
        }

    }

    private void getVolley(final String param) {
        // requête HTTP avec Volley
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        String url = String.format(Constant.URL_METEO, param, editTextCity.getText().toString());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String json) {

                        Log.e("json", "resultat: " + json);

                        if (param.equals(Constant.PARAM_CURRENT)) {
                            getDataCurrent(json);
                        } else if (param.equals(Constant.PARAM_FORECAST)) {
                            getDataForecast(json);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String json = new String(error.networkResponse.data);

                getDataCurrent(json);
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private void getDataCurrent(String json) {

        // parsing du flux JSON
        Gson myGson = new Gson();
        OpenWeatherMap openWeatherMap = myGson.fromJson(json, OpenWeatherMap.class);

        // condition si le cod est bien à 200
        if (openWeatherMap.getCod().equals("200")) {

            getVolley(Constant.PARAM_FORECAST);
            textViewCity.setText(openWeatherMap.getName());
            textViewTemperature.setText(openWeatherMap.getMain().getTemp() + " °C");

            String urlImage = String.format(Constant.URL_IMAGE, openWeatherMap.getWeather().get(0).getIcon());
            Picasso.get().load(urlImage)
                    .into(imageViewIcone);


        } else {
            FastDialog.showDialog(MainActivity.this, FastDialog.SIMPLE_DIALOG, openWeatherMap.getMessage());
        }
    }

    private void getDataForecast(String json) {
        // parsing du flux JSON
        Gson myGson = new Gson();
        OpenwMap5Days openwMap5Days = myGson.fromJson(json, OpenwMap5Days.class);

        if(openwMap5Days.getCod().equals("200")) {
            Log.e("result", openwMap5Days.getList().get(0).getMain().getTemp());

            // affichage des items dans la listview
            listViewForecast.setAdapter(new ForcastAdapter(MainActivity.this, R.layout.item_city, openwMap5Days.getList()));


        } else {
            FastDialog.showDialog(MainActivity.this, FastDialog.SIMPLE_DIALOG, openwMap5Days.getMessage());
        }
    }
}
