package com.example.meteo.models.current;

public class OpenWeatherMain {

    private String temp;

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
