package com.example.meteo.models.current;

import java.util.List;

public class OpenWeatherMap {

    private String name;
    private String cod;
    private String message;
    private OpenWeatherMain main;
    private List<OpenWeatherWeather> weather;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OpenWeatherMain getMain() {
        return main;
    }

    public void setMain(OpenWeatherMain main) {
        this.main = main;
    }

    public List<OpenWeatherWeather> getWeather() {
        return weather;
    }

    public void setWeather(List<OpenWeatherWeather> weather) {
        this.weather = weather;
    }

    @Override
    public String toString() {
        return "OpenWeatherMap{" +
                "name='" + name + '\'' +
                ", cod='" + cod + '\'' +
                ", message='" + message + '\'' +
                ", main=" + main +
                ", weather=" + weather +
                '}';
    }
}
