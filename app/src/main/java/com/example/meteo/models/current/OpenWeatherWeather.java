package com.example.meteo.models.current;

public class OpenWeatherWeather {

    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
