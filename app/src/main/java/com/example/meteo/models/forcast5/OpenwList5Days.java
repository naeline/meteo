package com.example.meteo.models.forcast5;



import java.util.List;

public class OpenwList5Days {

    private String dt_txt;
    private Main main;
    private List<Weather> weather;

    public class Main {
        private String temp;

        public String getTemp() {
            return temp;
        }
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public Main getMain() {
        return main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public class Weather {
        private String icon;

        public String getIcon() {
            return icon;
        }
    }

}
