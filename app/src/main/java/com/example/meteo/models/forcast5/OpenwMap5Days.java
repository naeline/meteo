package com.example.meteo.models.forcast5;

import java.util.List;

public class OpenwMap5Days {

    // meme facon de faire private String cod, message;
    private String cod;
    private String message;
    private List<OpenwList5Days> list;

    public String getCod() {
        return cod;
    }

    public String getMessage() {
        return message;
    }

    public List<OpenwList5Days> getList() {
        return list;
    }
}
