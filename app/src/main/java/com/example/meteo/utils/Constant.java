package com.example.meteo.utils;

public class Constant {


    public static final String URL_METEO = "https://api.openweathermap.org/data/2.5/%s?q=%s&units=metric&mode=json&appid=a749c68b1d6b747844f278d71e4c718d";
    public static final String URL_IMAGE = "https://openweathermap.org/img/w/%s.png";

    public static final String PARAM_CURRENT = "weather";  // permet de garder la meme url pour les 2 requetes et remplacer weather et forecast par %s
    public static final String PARAM_FORECAST = "forecast";// permet de factoriser un maximum et réduire le code
}
