package com.example.meteo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.meteo.models.forcast5.OpenwList5Days;
import com.example.meteo.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ForcastAdapter extends ArrayAdapter<OpenwList5Days> {   // entre chevrons il attend l objet qui sera répétée la partie list5days ici

    private int resId;

    public ForcastAdapter(Context context, int resource, List<OpenwList5Days> objects) {

        super(context, resource, objects);

        resId = resource;   // R.layout.item_city on le créer au dessus pour le réutiliser pour aller plus vite

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        convertView = LayoutInflater.from(getContext()).inflate(resId, null);
        TextView textViewDate = convertView.findViewById(R.id.textViewDate);
        TextView textViewTemperature = convertView.findViewById(R.id.textViewTemperature);
        ImageView imageViewIcone = convertView.findViewById(R.id.imageViewIcone);

        OpenwList5Days item = getItem(position);    // via la variable item que l'on va récuperer les elements de Openwlist5days

        // affichage de la date, temperature et image
        textViewDate.setText(item.getDt_txt());
        textViewTemperature.setText(item.getMain().getTemp() + " °C");
        Picasso.get().load(String.format(Constant.URL_IMAGE, item.getWeather().get(0).getIcon())).into(imageViewIcone);

        return convertView;
    }
}
